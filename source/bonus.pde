public class bonus {
  int xSpeedBonus;
  int xBonus = 1250;
  int yBonus;
  int bonusHeight = 60;
  int bonusWidth = 60;
  
  bonus() {
  xSpeedBonus = 10;
  }
  
  void drawBonus(){
    stroke(167, 48, 79);
    fill(167, 48, 79);
    strokeWeight(2);
    rect(xBonus, yBonus ,bonusWidth,bonusHeight);
    fill(244, 109, 141 );
    rect(xBonus + 10,yBonus + 10, bonusWidth - 20, bonusHeight - 20);
  }
  
  void move(){
  xSpeedBonus = -15;
  xBonus += xSpeedBonus;
  }
  
  void checkPosition(){
    if (xBonus < 0){
    xBonus = 1250;
    }
  }
  
}
